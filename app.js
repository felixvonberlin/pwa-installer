/*
PWA App Installer for iPadOS
Copyright (C) 2020 Felix v. Oertzen
pwaapp@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

iPadOS is a trademark of Apple Inc., registered in the U.S. and other countries.
*/
var query = document.location.search.substring(1);
var params = {};
var parts = query.split(/&/);
var t
for (var i in parts) {
  t = parts[i].split(/=/);
  params[decodeURIComponent(t[0])] = decodeURIComponent(t[1]);
}
const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);
if (isInStandaloneMode()){
  setTimeout(function () {
    window.top.location = params.app;
  },1);
  setTimeout(function () {
    document.getElementById("reopen").style.display = "block";
  },5000);
}

var appicon = params.app + "/apple-touch-icon.png";
if (params.app.includes("instagram.com"))
  appicon = "https://www.instagram.com/static/images/ico/favicon-200.png/ab6eff595bb1.png";
if (params.app.includes("discord.com"))
  appicon = "https://discord.com/assets/28174a34e77bb5e5310ced9f95cb480b.png";
if (params.app.includes("twitter.com"))
  appicon = "https://abs.twimg.com/responsive-web/client-web/icon-ios.8ea219d5.png";
if (params.appicon != "")
  appicon = params.appicon;

var meta_apple_touch_icon = document.createElement('link');
meta_apple_touch_icon.setAttribute("rel", "apple-touch-icon");
meta_apple_touch_icon.setAttribute("sizes", "180x180");
meta_apple_touch_icon.setAttribute("href", appicon);
document.getElementsByTagName('head')[0].appendChild(meta_apple_touch_icon);

var meta_apple_mobile_web_app_title = document.createElement('meta');
meta_apple_mobile_web_app_title.name = "apple-mobile-web-app-title";
meta_apple_mobile_web_app_title.content = params.appname;
document.getElementsByTagName('head')[0].appendChild(meta_apple_mobile_web_app_title);

var meta_application_name = document.createElement('meta');
meta_application_name.name = "application-name";
meta_application_name.content = params.appname;
document.getElementsByTagName('head')[0].appendChild(meta_application_name);

var meta_theme_color = document.createElement('meta');
meta_theme_color.name = "theme-color";
meta_theme_color.content = "#" + params.appcolor;
document.getElementsByTagName('head')[0].appendChild(meta_theme_color);

document.title = params.appname;

// === BELOW THE DOC ===

body = document.getElementsByTagName('body')[0];
body.style.background = "#" + params.appcolor;

i = document.getElementById('appicn');
i.src = appicon;

if(!isInStandaloneMode()) {
  d = document.getElementById('info');
  d.style.display = "block";
}
